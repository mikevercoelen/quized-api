import { Router } from 'express'
import Quiz from '../models/quiz'

const router = Router()

router.post('/quiz/:quizId', (req, res, next) => {
  const { quizId } = req.params

  Quiz.findByIdAndUpdate(quizId, {
    questions: req.body.questions
  }, {
    new: true
  }, (error, user) => {
    if (error) {
      return next(error)
    }

    res.sendStatus(200)
  })
})

router.post('/quiz', (req, res, next) => {
  const fullName = req.body.fullName

  let newQuiz = new Quiz()
  newQuiz.fullName = fullName
  newQuiz.questions = [{
    title: 'Wat is je favoriete versioning tool?',
    order: 0,
    answers: [
      { title: 'SVN', order: 0, score: 0 },
      { title: 'GIT', order: 1, score: 10 },
      { title: 'CVS', order: 2, score: -5 },
      { title: 'Mercurial', order: 3, score: 5 },
      { title: 'HUHH?', order: 4, score: 0 }
    ]
  }, {
    title: 'Wat is je favoriete cat?',
    order: 1,
    answers: [
      { title: 'Ceiling cat', order: 0, score: 0 },
      { title: 'Invisible bike cat', order: 1, score: 5 },
      { title: 'Octocat', order: 2, score: 12 },
      { title: 'Monorail cat', order: 3, score: 2 }
    ]
  }, {
    title: 'Wat is je favoriete edelsteen?',
    order: 2,
    answers: [
      { title: 'Diamant', order: 0, score: 5 },
      { title: 'Robijn', order: 1, score: 10 },
      { title: 'Kryptonite', order: 2, score: 10 },
      { title: 'Smaragd', order: 3, score: -5 }
    ]
  }]

  newQuiz.save((error) => {
    if (error) {
      return next(error)
    }

    res.json(newQuiz)
  })
})

export default router
