import async from 'async'
import config from 'nconf'
import logger from 'winston'

import loadConfig from './lib/load-config'

import initializeDatabase from './initializers/database'
import initializeServer from './initializers/server'
import initializeQuestions from './initializers/questions'

export default function start (done) {
  loadConfig()

  logger.info('Starting initialization')

  const onInitialized = (error) => {
    if (error) {
      logger.error('Initialization failed', error)
      return
    }

    logger.info('Initialized. Running on port: ' + config.get('PORT'))

    if (typeof done === 'function') {
      done()
    }
  }

  const initializeCore = (callback) => {
    async.series([
      initializeServer,
      initializeDatabase
    ], callback)
  }

  async.waterfall([
    (callback) => {
      initializeCore((error) => {
        if (error) {
          return callback(error)
        }

        callback()
      })
    },
    (callback) => {
      initializeQuestions(callback)
    }
  ], onInitialized)
}
