import config from 'nconf'

const CONFIG_FILE = __dirname + '/../config.json'

/**
 * Loads .env file, and initializes nconf
 */
export default function loadConfig (rootDir) {
  // setup nconf
  config.use('memory')
  config.argv()
  config.env()

  // load config json
  config.file(CONFIG_FILE)
}
