import requireDir from 'require-dir'

/**
 * Loads all routes with `app.use(x)` from a given directory
 *
 * @param  {string} routesDir path of the routes dir
 * @param  {object} app express app instance
 */
export default function loadRoutes (routesDir, app) {
  const routes = requireDir(routesDir, {
    recurse: true
  })

  Object.keys(routes).forEach((route) => {
    const router = routes[route]

    if (typeof router !== 'function') {
      throw new Error('Route file: routes/' + route + '.js did not return a router')
    }

    app.use(routes[route])
  })
}
