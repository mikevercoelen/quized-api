import { Schema } from 'mongoose'

/**
 * Returns a Mongoose Schema, but when data is outputted to JSON,
 * it will not include _id and __v, but it will have id.
 *
 * Why? If we expose _id, consumers will know that we use MongoDB.
 * So we hide it for security purposes.
 *
 * @param  {object} fields object of fields normally given to a mongoose schema
 * @return {object} instance of a mongoose schema
 */
export default function getMongooseSchema (fields) {
  let mongooseSchema = new Schema(fields)

  mongooseSchema.options.toJSON = {
    transform: function (doc, ret, options) {
      ret.id = ret._id
      delete ret._id
      delete ret.__v
      return ret
    }
  }

  return mongooseSchema
}
