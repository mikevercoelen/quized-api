import logger from 'winston'
import config from 'nconf'

/**
 * Very basic error handler for express.
 * @return {function} express middleware
 */
export default function errorHandler () {
  const env = config.get('env')

  return function errorHandler (error, req, res, next) {
    logger.error('Route error', error)

    res.status(error.status || 500)

    res.json({
      message: error.message,
      error: (env === 'development' ? error : {})
    })
  }
}
