import mongoose from 'mongoose'
import getMongooseSchema from '../lib/get-mongoose-schema'

export const answerSchema = getMongooseSchema({
  title: {
    type: String,
    required: true
  },
  order: {
    type: Number,
    required: true
  },
  score: {
    type: Number,
    required: true
  },
  selected: {
    type: Boolean,
    default: false
  }
})

export const questionSchema = getMongooseSchema({
  title: {
    type: String,
    required: true
  },
  order: {
    type: Number,
    required: true
  },
  answers: [answerSchema]
})

export const quizSchema = getMongooseSchema({
  fullName: {
    type: String,
    required: true
  },
  questions: [questionSchema]
})

const QuizModel = mongoose.model('Quiz', quizSchema)

export default QuizModel
