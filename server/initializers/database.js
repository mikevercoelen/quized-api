import config from 'nconf'
import mongoose from 'mongoose'

/**
 * Initialize database using mongoose
 * @param  {Function} callback
 */
export default function initializeDatabase (callback) {
  mongoose.connect(config.get('MONGO_URL'), callback)
}
