import config from 'nconf'
import morgan from 'morgan'
import express from 'express'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import cors from 'cors'

import loadRoutes from '../lib/load-routes'

import errorHandler from '../middleware/error-handler'

const ROOT_DIR = __dirname + '/..'
const ROUTES_DIR = ROOT_DIR + '/routes'

export const app = express()

const corsOptions = {
  origin: '*',
  methods: ['GET', 'PUT', 'POST', 'DELETE'],
  allowedHeaders: ['X-Requested-With', 'Content-Type']
}

/**
 * Initializes app server, loads middleware etc.
 * @param  {Function} callback callback
 */
export default function initializeServer (callback) {
  config.set('env', app.get('env'))

  // don't send "x-powered-by" header (security)
  app.disable('x-powered-by')

  // initialize middleware
  app.options('*', cors(corsOptions))
  app.use(cors(corsOptions))
  app.use(morgan('common'))
  app.use(cookieParser())
  app.use(bodyParser.json())
  loadRoutes(ROUTES_DIR, app)
  app.use(errorHandler())

  // start
  app.listen(config.get('PORT'), callback)
}
