# Requirements

- MongoDB (config can be found in `/server/config.json`)

# Running it

```shell
npm install
npm start
```

# Running tests

```shell
npm test
```
