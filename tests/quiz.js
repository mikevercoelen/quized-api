import chai from 'chai'
import chaiHttp from 'chai-http'
import { PORT } from '../server/config.json'

const URL = 'http://localhost:' + PORT

const expect = chai.expect;
const should = chai.should()

chai.use(chaiHttp)

describe('Quiz', () => {
  let quiz = {}

  it('should successfully POST /quiz and return JSON object with all the questions and answers', (done) => {
    const fullName = 'Mr. Anderson'

    chai
      .request(URL)
      .post('/quiz')
      .send({ fullName })
      .end((error, res) => {
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('fullName')
        res.body.should.have.property('questions')
        res.body.should.have.property('id')
        quiz = res.body
        done()
      })
  })

  it('should successfully POST /quiz/:quizId (update an answer)', (done) => {
    const quizId = quiz.id
    let newQuiz = quiz

    // update first answer of the first question to true
    newQuiz.questions[0].answers[0].selected = true

    chai
      .request(URL)
      .post('/quiz/' + quizId)
      .send({ questions: newQuiz.questions })
      .end((error, res) => {
        res.should.have.status(200)
        done()
      })
  })
})
