import chai from 'chai'
import chaiHttp from 'chai-http'
import { PORT } from '../server/config.json'

const URL = 'http://localhost:' + PORT

const expect = chai.expect;
const should = chai.should()

chai.use(chaiHttp)

describe('Test', () => {
  it('GET /test - should return JSON object with "hello world" message', (done) => {
    chai
      .request(URL)
      .get('/test')
      .end((error, res) => {
        res.should.be.json
        res.body.should.be.a('object')
        res.body.should.have.property('message')
        res.body.message.should.equal('hello world')
        done()
      })
  })
})
